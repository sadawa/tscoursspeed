//Not good
// const user : (string|number)[] = ["j",15]

//Good
let user: [string, number, boolean];

user = ["j", 150, true];
//Order is important tuples
// user = [150,false,'j']

let rgb: [number, number, number] = [255, 123, 112];

type User = [number, string];

const newUser: User = [112, "exemple@test.com"];

newUser[1] = "t.com";
newUser.push(true);

//enum
enum Seatchoice {
  Asile,
  MIDDLE,
  WINDOW,
  FOURTH,
}

const hcSeat = Seatchoice.Asile;
