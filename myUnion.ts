//Create union |
let score: number | string = 33;

// number
score = 55;

// string
score = "55";

type User = {
  name: string;
  id: number;
};

type Admin = {
  username: string;
  id: number;
};

// User
let person: User | Admin = { name: "John", id: 350 };

//Admin
person = { username: "John", id: 350 };

function getDbId(id: number | string) {
  console.log(`Db id is : ${id}`);
}

// Array

// Not good
// const data: number[] = [1, 2, 3, "4"];

//Not good
// const data2: string[] = [1, 2, 3, "4"];

// Use Array union good
const data3: (string | number)[] = [1, 2, 3, "4"];
