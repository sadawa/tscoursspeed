class User {
  private _courseCount = 1;

  email: string;
  name: string;
  constructor(email: string, name: string) {
    this.email = email;
    this.name = name;
  }
  //Getter
  get getAppleEmail(): string {
    return `apple${this.email}`;
  }
  get courseCount(): number {
    return this._courseCount;
  }
  //Setters
  set courseCount(courseNum) {
    if (courseNum <= 1) {
      throw new Error("You must have at least one course");
    } else {
      this._courseCount = courseNum;
    }
  }
}

const test = new User("john@gmail.com", "john");
