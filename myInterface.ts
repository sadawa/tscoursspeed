//Interface
interface User {
  readonly dbId: number;
  email: string;
  userId: number;
  googleId?: string;
  // startTrail: () => string
  startTrail(): string;
  getCoupon(couponName: string): number;
}

const shiro: User = {
  dbId: 22,
  email: "j@test.com",
  userId: 1203,
  startTrail: () => {
    return "trail started";
  },
  getCoupon: (name: "Hello10") => {
    return 10;
  },
};

shiro.email = "s@test.com";
