interface TakePhto {
  cameraMode: string;
  filter: string;
  burst: number;
}

class Instagram implements TakePhto {
  constructor(
    public cameraMode: string,
    public filter: string,
    public burst: number
  ) {}
}
