// const User = {
//     name: 'John Doe',
//     age: 30,
//     email: 'john@gmail.com',
//     isActive : true
// }

//Create object type
type User = {
  readonly _id: string;
  name: string;
  age: number;
  email: string;
  isActive: boolean;
};

// function createUser ({name:string,isActive: boolean,age : number}){}

function createUser(user: User) {
  return { name: "", age: "", isActive: true, email: "" };
}

createUser({
  name: "John Doe",
  isActive: false,
  age: 25,
  email: "John@test.fr",
  _id: "123456",
});

let myUser: User = {
  _id: "123",
  name: "John Doe",
  age: 30,
  email: "john@gmail.com",
  isActive: true,
};

myUser.email = "j@test.com";

function createCourse(): { name: string; isActive: boolean } {
  return { name: "reacts", isActive: true };
}
