// good partice function
function addNumber(num: number): number {
  return num + 2;
}

function getUpper(val: string) {
  return val.toUpperCase();
}

function signUp(name: string, email: string, password: string) {}

let loginUser = (username: string, password: string) => {
  console.log(username);
  console.log(password);
};

addNumber(5);
getUpper("Hello");
signUp("john", "john@gmail.com", "123456789");
loginUser("john", "john@gmail.com");

// function getValue(myVal:number): boolean{
//   if(myVal > 5 ){
//     return myVal;
//   }
//   return "200 Ok"
// }

//good partice arrow function
const getHello = (s: string): string => {
  return "";
};

//good partice map
const heros = ["naruto", "yuji", "goku", "ichigo", "luffy"];

heros.map((hero): string => {
  return `hero is ${hero}`;
});

//good partice for console error or console.log
function consoleError(errmsg: string): void {
  console.log(errmsg);
}

function handleError(errmsg: string): never {
  throw new Error(errmsg);
}

export {};
