function logValue(x: Date | string) {
  if (x instanceof Date) {
    console.log(x.toUTCString());
  } else {
    console.log(x.toUpperCase());
  }
}

type Fish = { swim: () => void };
type Bird = { fly: () => void };

function isFish(pet: Fish | Bird) {
  (pet as Fish).swim !== undefined;
}

interface Circle {
  kind: "cirlce";
  radius: number;
}

interface Square {
  kind: "square";
  side: number;
}

interface Rectangle {
  kind: "rectangle";
  width: number;
  height: number;
}

type Shape = Circle | Square | Rectangle;

function getTrueShape(shape: Shape) {
  if (shape.kind === "cirlce") {
    return { kind: "cirlce", radius: shape.radius };
  } else if (shape.kind === "square") {
    return { kind: "square", side: shape.side };
  } else {
    return { kind: "rectangle", width: shape.width, height: shape.height };
  }
}
